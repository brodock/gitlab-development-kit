worker_processes  1;
daemon off;
pid tmp/nginx.pid;
error_log stderr;

events {
  worker_connections  1024;
}


http {
  include       mime.types;
  default_type  application/octet-stream;

  access_log logs/access.log;

  sendfile        on;
  #tcp_nopush     on;

  #keepalive_timeout  0;
  keepalive_timeout  65;

  #gzip  on;
  proxy_temp_path proxy_temp;
  client_body_temp_path client_body_temp;
  uwsgi_temp_path uwsgi_temp;
  fastcgi_temp_path fastcgi_temp;
  scgi_temp_path scgi_temp;

  client_max_body_size 0;

  upstream gitlab {
    server unix:/home/git/gitlab.socket fail_timeout=0;
  }

  upstream gitlab-workhorse {
    server unix:/home/git/gitlab-workhorse.socket fail_timeout=0;
  }

  server {
    listen       3000;
    server_name  localhost;

    location / {
        try_files $uri $uri/index.html @uri.html @gitlab;
    }

    location ~ ^/[\w\.-]+/[\w\.-]+/(info/refs|git-upload-pack|git-receive-pack)$ {
      error_page 418 = @gitlab-workhorse;
      return 418;
    }

    location ~ ^/[\w\.-]+/[\w\.-]+/repository/archive {
      error_page 418 = @gitlab-workhorse;
      return 418;
    }

    location ~ ^/api/v3/projects/.*/repository/archive {
      error_page 418 = @gitlab-workhorse;
      return 418;
    }

    location @gitlab-workhorse {
        proxy_set_header    Host                $http_host;
        proxy_set_header    X-Real-IP           $remote_addr;
        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto   $scheme;

        proxy_pass http://gitlab-workhorse;
    }

    location @gitlab {
      proxy_redirect      off;
      proxy_set_header    Host                $http_host;
      proxy_set_header    X-Real-IP           $remote_addr;
      proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
      proxy_set_header    X-Forwarded-Proto   $scheme;
      proxy_set_header    X-Frame-Options     SAMEORIGIN;

      proxy_pass http://gitlab;
    }
  }
}
